CAB302 Software Development
===========================

# Practical 4: Collections #

This week's practical exercise will give you experience at designing and implementing your own extension to the standard Java Collections Framework.

* * *

## Background ##

Bags, also known as _multisets_, are a data structure consisting of an _unordered_ collection of elements in which _duplicate elements_ are allowed. 
This means that not only can we ask whether an element occurs in the collection, as we can with sets, but we can also ask how many times it does so.

According to one of Oracle's Frequently Asked Questions pages, the standard Java Collections Framework does not include bags as a type because they are not needed very often. 
(The fact that Oracle is _frequently_ asked about bags makes one wonder if this is true!) 
Fortunately, we can easily implement our own bag type if we need them. Indeed, there are many different Java bag implementations on the web and the Java Collection API specification even mentions the need or them.

Our overall goal is therefore to add a bag type to the Java collections framework. 
A JUnit test class file `BagTest.java` has been provided to help you get started (although you will need to modify these tests to keep up with your implementation). 
An exception class file `BagException.class` has also been provided for use in your solution.

* * *

## Exercise 1: Designing the type ##

Before beginning the implementation you need to decide exactly what operations your bag class will provide and how it links into the existing Collections Framework.

Consider the interface classes at the top level of Java's Collections Framework, i.e., the `Collection`, `Set`, `List`, `Map` and `Queue` types (plus the `Array` utilities).  
Given the characteristics of a bag described above, underneath which class in this hieararchy would a `Bag` class belong? 
Keep in mind the principles of subtyping -- a subtype must be usable wherever its supertype is expected.
In particular, is a multiset (i.e., a bag) usable where a set is expected?

You also need to consider which operations are essential to allow the new type to be used successfully. 
We suggest that the following operations are especially useful for bags:

* `add(e, n)` -- adds _n_ copies of element _e_ to the bag,
* `remove(e, n)` -- removes _n_ copies of element _e_ from the bag,
* `quantity(e)` -- returns the number of copies of element _e_ in the bag (which may be zero),
* `toSet()` -- returns a set comprising all of the distinct elements in the bag (i.e. with no duplicates),
* `size()` -- returns the number of elements in the bag, counting duplicates, and
* `iterator()` -- returns an iterator object for bags (which will support 'for each' style **for** loops involving bags).

Study the supplied unit tests to see exactly how these methods are used under normal circumstances. 
(We have not supplied an API document with this exercise because this would reveal aspects of the type hierarchy that we want you to design for yourself.)

**NB:** The supplied set of unit tests is not complete, because tests for boundary cases and exceptional situations depend on design decisions that you have to make. 
For instance, what should the `remove` method do if asked to remove more copies of a particular element than are in the bag? What should the add method do if asked to `add` a negative number of items?

* * *

## Exercise 2: Specifying the type's methods ## 

As we have seen, a common idiom in Java's Collections Framework is to define an interface or abstract class to specify the methods available for a particular type, and to then provide one or more concrete classes that implement these methods, each using a different data structure for the underlying representation. 
For this exercise we will do the same, starting with an abstract `Bag` class which specifies the operations available for bags.

Recall from the lecture demonstration that an implementation of an interface or abstract class is obliged to implement _all_ of the methods specified in the supertype, of which there may be many. 
To save effort, Java's Collections Framework provides abstract supertype classes that have default dummy implementations for most non-essential methods. 
These include `AbstractCollection`, `AbstractSet`, `AbstractList`, `AbstractMap` and `AbstractQueue`. 
Your `Bag` class should extend one of these classes.

Having chosen to build your `Bag` type under one of these existing classes, you will construct a type hierarchy as shown in the *Type Hierarchy* image.

![Type Hierarchy](imgs/hierarchy.png "Type Hierarchy")

The goal of this exercise is to define the abstract class `Bag` which defines the methods that the bag type must provide, but doesn't implement them. 
Create a new class called `Bag` in Eclipse, which extends your chosen superclass, and populate it with specifications for all of the operations peculiar to bags. 
Note that you don't have to provide specifications for any methods already provided by the superclass. 
(However, depending on which superclass you use for your `Bag` class, you may be obliged to implement methods other than those listed above.)
Eclipse's _Hierarchy_ view ![Type Hierarchy](imgs/hierarchyicon.png) is useful for viewing the design of your type hierarchy in the context of the standard Collections Framework types.

* * *

## Exercise 3: Implementing the type ##

As the figure above shows, given an abstract specification of a class, we can usually implement it in several different ways. 
In this exercise you are required to extend your `Bag` specification with a concrete class that implements each of the bag operations, using a particular data structure to represent bags, chosen from the standard Collections Framework types. 
For instance, an implementation based on the standard `LinkedList` class would be called `LinkedBag`, one based on the `HashMap` class would be called `HashBag`, and so on.

**Hint:** Most of the methods listed above are straightforward to understand and implement.
The `iterator()` method is less obvious, however. 
In general an iterator method must return an object with three operations, `hasNext()`, `next()` and `remove()`, that allows us to traverse the elements of a collection. 
(See the JDK API entry for the `Iterator` class for more detail.) 
Although you could choose to do this explicitly, by defining a nested class with these three methods, a much easier option for implementing the `iterator()` method is to simply put your bag's contents in an object of one of the standard data types (ensuring that it is one that preserves duplicates) and then return that object's iterator.

For the purposes of this practical you should attempt to make use of the provided `BagException` class to throw appropriate exceptions in your implementation. 
If you are having difficulty with your `Bag` implementation you should concentrate on a simple implementation without exceptions first and then add exceptions once you have it working.

* * *

## Exercise 4: Testing the type ##

While developing your new type you should test its functionality with the supplied `BagTest` class to measure your progress in implementing the operations. 
However, you may need to modify some of the tests to reflect design decisions you have made. 
In particular, if your methods may throw exceptions you will need to add __`throws`__ declarations to the corresponding unit tests.

e.g. `public void singleElementType()` **`throws BagException`** `{`
    
Furthermore, you should add new unit tests to reflect design decisions you have made. 
In particular, the supplied tests don't check any unusual cases, such as being asked to add a negative number of elements, or remove more elements than the bag contains, so you should add tests that reflect how you chose to resolve these issues.

* * *

## Exercise 5: Documenting the type ##

Having implemented a new data type you now need to document it so that other programmers know how to use it. 
Use Javadoc comments to describe your new type and its methods and then generate an API specification for the new type. 
Note that Javadoc comments you put on method specifications in the `Bag` class will be automatically copied
to their overriding implementations in the concrete class when the API document is generated, so you don't need to copy them into both source code files.

* * *

## Exercise 6: Efficiency ##

A simple, and naïve, implementation of an iterator for the `Bag` would be to copy the contents of the `Bag` into some type of `Collection` and then return an iterator to that collection. 
For a `Bag` with a large number of elements this is very inefficient. 
(To test this try creating a `Bag` with a billion elements in it.)
Create an `Iterator` class for your implementation of the `Bag` that iterates over the contents of the bag without copying the elements into another collection. 
Your class will need to implement the `Iterator` interface.
