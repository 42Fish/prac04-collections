package answers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class HashBag<T> extends Bag<T> {
	
	private HashMap<T, Integer> map;
	
	public HashBag(){
		map = new HashMap<T, Integer>();
	}
	
	
	@Override
	public void add(T element, int n) {
		// TODO Auto-generated method stub
		
		Integer num = map.get(element);
		if (num==null){
			map.put(element, n);
		} 
		else {
			map.put(element, n + num);
		}
		
	}

	@Override
	public void remove(T element, int n) {
		// TODO Auto-generated method stub
		
		Integer num = map.get(element);
		if (num==null){
			
		} else {
			map.put(element, num - n);
		}
		
	}

	@Override
	public int quantity(T element) {
		// TODO Auto-generated method stub
		Integer num = map.get(element);
		if (num==null){
			return 0;
		} else {
			return num;
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		int sum = 0;
		for ()
		return 0;
	}

	@Override
	public Set<T> toSet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

}
