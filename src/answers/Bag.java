package answers;

import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.Set;

public abstract class Bag<T> extends AbstractCollection<T> {
	//HashBag<E>;
	//LinkedBag<E>;
	/*	�add(e, n) 
		�remove(e, n) 
		�quantity(e) 
		�toSet() 
		�size()
		�iterator()
	 */
	
	//Adds n copies of element e to the bag.
	public abstract void add(T element, int n);
	//Removes n copies of element e from the bag.
	public abstract void remove(T element, int n);
	//Returns the number of copies of element e in the bag (which may be zero).
	public abstract int quantity(T element);
	//Returns the number of elements in the bag, counting duplicates.
	public abstract int size();
	//Returns a set comprising all of the distinct elements in the bag (i.e. with no duplicates).
	public abstract Set<T> toSet();
	//Returns an iterator object for bags (which will support 'for each' style for loops involving bags).
	public abstract Iterator<T> iterator();
}
